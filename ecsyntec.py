#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/1/3 2:02
# @Author  : leon yan
# @Site    : 
# @File    : Ecsyntec.py
# @Software: PyCharm


from PyQt5.Qt import QWidget, pyqtSlot, QApplication, QTimer, QMessageBox

from resource.Ecsyntec_ui import Ui_Form

import sys


import clr
sys.path.append(r'.\API')
clr.FindAssembly('Syntec.RemoteCNC.dll')
clr.AddReference('Syntec.RemoteCNC')

from Syntec.Remote import *

class syntecForm(QWidget, Ui_Form):
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.setupUi(self)
        Qpulse = QTimer(self)
        Qpulse.timeout.connect(self.do_timer)
        Qpulse.start(500)
        self.clockpulse = False
        self.clockEnable = False
        self.btnTimeOn.setEnabled(True)
        self.btnTimeOff.setEnabled(False)

        self.cnc = []
        self.cnc.append('192.168.31.129')
        self.cnc.append('192.168.31.224')
        self.cnc.append('192.168.31.224')
        self.cnc.append('192.168.31.224')
        for i in range(1, 5, 1):
            exec("self.host" + str(i) + ".setText('" + (self.cnc[i - 1]) + "')")

        for i in range(5):
            exec("self.labelRa" + str(i) + ".setText('""')")
            exec("self.labelRb" + str(i) + ".setText('""')")
            exec("self.labelRc" + str(i) + ".setText('""')")
            exec("self.labelRd" + str(i) + ".setText('""')")

    def __del__(self):
        print("Ec_syntec_test is deleted")

    def do_timer(self):
        if self.clockEnable:
            if self.clockpulse:
                self.clockpulse = False
                self.labelcomm.setStyleSheet("")
            else:
                self.clockpulse = True
                self.labelcomm.setStyleSheet("background:#69b518")
                self.do_comm()
        else:
            self.labelcomm.setStyleSheet("")
            for i in range(1, 5, 1):
                exec("self.error" + str(i) + ".setText('')")

    def do_comm(self):
        self.cnc[0] = self.host1.text()
        self.cnc[1] = self.host2.text()
        self.cnc[2] = self.host3.text()
        self.cnc[3] = self.host4.text()

        errLst = []
        for i in range(4):
            aa = SyntecRemoteCNC()
            aa.Host = self.cnc[i]
            SyntecRemoteCNC(aa.Host)
            aa2 = aa.READ_position('', 0, '', '', '', '', '')
            exec("self.error" + str(i + 1) + ".setText('" + str(aa2[0]) + "')")
            if aa2[0] == 0:
                aa3 = aa.READ_plc_addr("R", 26, 30, '0', '0', '0', '0')
                plcdata = aa3[4]
                if plcdata:
                    for j in range(5):
                        if i == 0:
                            exec("self.labelRa" + str(j) + ".setText('"+str(plcdata[j])+"')")
                        if i == 1:
                            exec("self.labelRb" + str(j) + ".setText('"+str(plcdata[j])+"')")
                        if i == 2:
                            exec("self.labelRc" + str(j) + ".setText('"+str(plcdata[j])+"')")
                        if i == 3:
                            exec("self.labelRd" + str(j) + ".setText('"+str(plcdata[j])+"')")
            else:
                for j in range(5):
                    if i == 0:
                        exec("self.labelRa" + str(j) + ".setText('NG')")
                    if i == 1:
                        exec("self.labelRb" + str(j) + ".setText('NG')")
                    if i == 2:
                        exec("self.labelRc" + str(j) + ".setText('NG')")
                    if i == 3:
                        exec("self.labelRd" + str(j) + ".setText('NG')")
            # errLst.append(aa2)

    @pyqtSlot()
    def on_btnTimeOn_clicked(self):
        self.clockEnable = True
        self.btnTimeOn.setEnabled(False)
        self.btnTimeOff.setEnabled(True)

    @pyqtSlot()
    def on_btnTimeOff_clicked(self):
        self.clockEnable = False
        self.btnTimeOn.setEnabled(True)
        self.btnTimeOff.setEnabled(False)

    @pyqtSlot()
    def on_btnWrite1_clicked(self):
        if self.ncname1.text() == "":
            QMessageBox.warning(self, "提示", "文件名不可为空!")
            return
        aa = SyntecRemoteCNC()
        aa.Host = self.host1.text()
        dd = aa.WRITE_nc_main(self.ncname1.text())
        self.error1.setText(str(dd))

    @pyqtSlot()
    def on_btnWrite2_clicked(self):
        if self.ncname2.text() == "":
            QMessageBox.warning(self, "提示", "文件名不可为空!")
            return
        aa = SyntecRemoteCNC()
        aa.Host = self.host2.text()
        dd = aa.WRITE_nc_main(self.ncname2.text())
        self.error2.setText(str(dd))

    @pyqtSlot()
    def on_btnWrite3_clicked(self):
        if self.ncname3.text() == "":
            QMessageBox.warning(self, "提示", "文件名不可为空!")
            return
        aa = SyntecRemoteCNC()
        aa.Host = self.host3.text()
        dd = aa.WRITE_nc_main(self.ncname3.text())
        self.error3.setText(str(dd))

    @pyqtSlot()
    def on_btnWrite4_clicked(self):
        if self.ncname4.text() == "":
            QMessageBox.warning(self, "提示", "文件名不可为空!")
            return
        aa = SyntecRemoteCNC()
        aa.Host = self.host4.text()
        dd = aa.WRITE_nc_main(self.ncname4.text())
        self.error4.setText(str(dd))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    syntec_Form = syntecForm()

    syntec_Form.show()
    sys.exit(app.exec_())
