# PlcApplet-master

#### 介绍
相关Plc,Cnc等工业设备的交互工具
ecsyntec.py:这是使用Python与新代的C# api进行交互的简单项目，以后将逐步丰富交互内容

#### 软件架构
使用python 3.7.6 32位

#### 安装教程

1.  直接复制到Pc机上
2.  关闭360或者添加信任

#### 使用说明

1.  设定需要连接的设备地址，最多4个
2.  进行通信测试，若显示为-16表示通信故障。请检测Ip设定和以太网线
3.  测试完毕后可关闭通信检测按钮
4.  输入事先拷贝在设备上的加工文件，在输入框中输入相应的文件名，按写入，检查当前的加工文件名是否有变化。

#### 参与贡献

1.  新代C# API接口及相应文档资料


#### 瑞项科技
